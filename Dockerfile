# syntax=docker/dockerfile:1.2

# To understand how this image works, please refer to YAMDI:
# https://gitlab.com/CodingKoopa/yamdi/-/blob/main/Dockerfile

# hadolint ignore=DL3006
FROM alpine

ARG PACKAGES

# hadolint ignore=DL3018
RUN \
  set -eux; \
  apk update; \
  apk upgrade; \
  apk add --no-cache $PACKAGES; \
  rm -rf /var/cache/apk/*;
